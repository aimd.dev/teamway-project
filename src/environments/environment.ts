// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'tw-project-99ba3',
    appId: '1:536838749130:web:f5cbc3db87c9ae0b796d33',
    storageBucket: 'tw-project-99ba3.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyAMyw8gpRZhrNcA5OzWDfgo6xmWoTMkLv4',
    authDomain: 'tw-project-99ba3.firebaseapp.com',
    messagingSenderId: '536838749130',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
