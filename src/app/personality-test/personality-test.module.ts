import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PersonalityTestRoutingModule } from './personality-test-routing.module';
import { SharedComponentsModule } from '../shared/components/shared-components.module';

import { PersonalityTestHomeComponent } from './pages/personality-test-home/personality-test-home.component';
import { PersonalityTestQuestionnaireComponent } from './pages/personality-test-questionnaire/personality-test-questionnaire.component';
import { QuestionStepperComponent } from './components/question-stepper/question-stepper.component';
import { RadioComponent } from './components/radio/radio.component';



@NgModule({
  declarations: [
    PersonalityTestHomeComponent,
    PersonalityTestQuestionnaireComponent,
    QuestionStepperComponent,
    RadioComponent
  ],
  imports: [
    CommonModule,
    PersonalityTestRoutingModule,
    SharedComponentsModule,
    ReactiveFormsModule
  ]
})
export class PersonalityTestModule { }
