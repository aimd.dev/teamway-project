import { Injectable } from '@angular/core';
import { collection, Firestore, getDocs, query } from '@angular/fire/firestore';
import { doc, getDoc } from '@firebase/firestore';
import { from, map, tap } from 'rxjs';
import { Questionnaire } from '../shared/models';

@Injectable({
  providedIn: 'root'
})
export class PersonalityTestService {
  /**
   * Service constructor
   * @param firestore db
   */
  constructor(
    private firestore: Firestore
  ) { }
  /**
   * Get all tests
   */
  getQuestionnaires() {
    return from(getDocs(query(collection(this.firestore, 'questionnaires'))))
    .pipe(
      map(response => response.docs.map(doc => ({ id: doc.id, ...doc.data() } as Questionnaire))),
      tap(questionnaires => console.log(questionnaires))
    );
  }
  /**
   * Get a test by id
   * @param id test id
   */
  getQuestionnaire(id: string) {
    return from(getDoc(doc(this.firestore, `questionnaires/${id}`)))
    .pipe(
      map(response => ({ id: response.id, ...response.data() } as Questionnaire)),
    );
  }
}
