import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Questionnaire } from 'src/app/shared/models';

@Component({
  selector: 'app-personality-test-home',
  templateUrl: './personality-test-home.component.html',
  styleUrls: ['./personality-test-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalityTestHomeComponent implements OnInit {
  /**
   * Test selection form
   */
  form!: FormGroup;
  /**
   * Questionnaires stream
   */
  questionnaires!: Questionnaire[];
  /**
   * Component constructor
   * @param router angular router
   * @param fb angular form builder
   * @param service module service
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }
  /**
   * Component on init
   */
  ngOnInit(): void {
    this.form = this.fb.group({
      questionnaire: ['', [Validators.required]]
    });
    this.questionnaires = this.route.snapshot.data['questionnaires'];
  }
  /**
   * Selected questionnaire control
   */
  get q () { return this.form.get('questionnaire') as FormControl; }
  /**
   * Submit event handler
   */
  submit() {
    if (this.form.invalid) { return; }
    this.router.navigate([`tests/${this.q.value}`], {state: this.questionnaires.find(q => q.id === this.q.value)});
  }
}
