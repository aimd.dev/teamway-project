import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Questionnaire, Result } from 'src/app/shared/models';
import { QuestionStepperComponent } from '../../components/question-stepper/question-stepper.component';

@Component({
  selector: 'app-personality-test-questionnaire',
  templateUrl: './personality-test-questionnaire.component.html',
  styleUrls: ['./personality-test-questionnaire.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalityTestQuestionnaireComponent implements OnInit {
  /**
   * Current questionnaire
   */
  questionnaire!: Questionnaire;
  /**
   * Test result
   */
  result$!: Observable<Result | undefined>;
  /**
   * Stepper component
   */
  @ViewChild('stepper') stepper!: QuestionStepperComponent;
  /**
   * Component constructor
   * @param route activated route
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }
  /**
   * Component on init
   */
  ngOnInit(): void {
    this.questionnaire = this.route.snapshot.data['questionnaire'];
    // Result listener
    this.result$ = this.route.queryParams.pipe(
      map(params => {
        const id = params['result'];
        return this.questionnaire.results.find(r => r.id === id);
      })
    );
  }
  /**
   * Handle results
   */
  finishQuestionnaire(result: string) {
    this.router.navigate(['.'], { queryParams: { result }, relativeTo: this.route });
  }
  /**
   * Restart the test
   */
  restart() {
    this.stepper.reset();
    this.router.navigate(['.'], { relativeTo: this.route });
  }
}
