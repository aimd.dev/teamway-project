import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionnaireListResolver } from '../core/resolvers/questionnaire-list.resolver';
import { QuestionnaireResolver } from '../core/resolvers/questionnaire.resolver';

import { PersonalityTestHomeComponent } from './pages/personality-test-home/personality-test-home.component';
import { PersonalityTestQuestionnaireComponent } from './pages/personality-test-questionnaire/personality-test-questionnaire.component';

const routes: Routes = [
  {
    path: '',
    component: PersonalityTestHomeComponent,
    resolve: { questionnaires: QuestionnaireListResolver }
  },
  {
    path: 'tests/:questionnaire',
    component: PersonalityTestQuestionnaireComponent,
    resolve: { questionnaire: QuestionnaireResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalityTestRoutingModule { }
