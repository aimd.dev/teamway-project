import { ChangeDetectionStrategy, Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { Option } from 'src/app/shared/models';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioComponent),
      multi: true,
    },
  ],
})
export class RadioComponent implements ControlValueAccessor {
  /**
   * Selected option index
   */
  checked$: Subject<number> = new Subject<number>();
  /**
   * Input for radio option list
   */
  @Input() options!: Option[];
  /**
   * Control value accessor implementation
   */
  propagateChange = (_: any) => {};
  propagateTouched = () => {};
  public registerOnChange(fn: (value: any) => any): void {
    this.propagateChange = fn;
  }
  public registerOnTouched(fn: () => void): void {
    this.propagateTouched = fn;
  }
  public writeValue(value: any) {
    this.checked$.next(value);
  }
  /**
   * Radio select event handler
   * @param index option index
   */
  select(index: number) {
    this.checked$.next(index);
    this.propagateChange(index);
  }
}
