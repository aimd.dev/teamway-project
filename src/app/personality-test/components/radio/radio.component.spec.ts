import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioComponent } from './radio.component';

describe('RadioComponent', () => {
  let component: RadioComponent;
  let fixture: ComponentFixture<RadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render options', () => {
    const options = [
      {body: 'sample', resultId: 'A'},
      {body: 'sample', resultId: 'B'}
    ];
    component.options = [...options];
    fixture.detectChanges();
    const radios = fixture.nativeElement.querySelectorAll('.radio');
    expect(radios.length).toBe(options.length);
  });
});
