import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Question } from 'src/app/shared/models';

@Component({
  selector: 'app-question-stepper',
  templateUrl: './question-stepper.component.html',
  styleUrls: ['./question-stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionStepperComponent {
  /**
   * Local form
   */
  form!: FormGroup;
  /**
   * Active question
   */
  active$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  /**
   * Question list
   */
  questionList!: Question[];
  /**
   * Configure local form with the received question list
   */
  @Input() set questions(questions: Question[]) {
    const controls = questions.reduce((result, next, i) => ({ ...result, [i]: new FormControl('', Validators.required) }), {});
    this.form = this.fb.group(controls);
    this.questionList = [ ...questions ];
  }
  /**
   * Questionnaire finished event
   */
  @Output() results: EventEmitter<string> = new EventEmitter<string>();
  /**
   * Component constructor
   * @param fb angular form builder
   */
  constructor(
    private fb: FormBuilder
  ) { }
  /**
   * Question navigation
   * @param dir direction
   */
  navigate(dir: number) {
    const current = this.active$.getValue();
    const next = current + dir;
    // Min page validation
    if (next < 0) { return; }
    // Test end
    if (next === this.questionList.length) {
      this.calculateResult();
      return;
    }
    // Default navigation
    this.active$.next(next);
  }
  /**
   * Calculate the result with the selected options
   */
  calculateResult() {
    // Get result ids
    const result = Object.keys(this.form.value).map((k) => {
      const selected = (this.form.get(k) as FormControl).value;
      return this.questionList[k as any].options[selected].resultId;
    });
    // Count
    const store = result.reduce((res, next) => ({
      ...res, [next]: res[next] ? (res[next] + 1) : 1
    }), {} as any);
    // Select highest value
    const highest = Object.keys(store).sort((a, b) => store[b] - store[a])[0];
    this.results.emit(highest);
  }
  /**
   * Reset the form
   */
  reset() {
    this.form.reset();
  }
}
