import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WaveComponent } from './wave/wave.component';
import { LogoComponent } from './logo/logo.component';



@NgModule({
  declarations: [
    WaveComponent,
    LogoComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    WaveComponent,
    LogoComponent
  ]
})
export class SharedComponentsModule { }
