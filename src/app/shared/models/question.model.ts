import { Option } from './option.model';

export interface Question {
    body: string;
    options: Option[];
}
