import { Question } from './question.model';
import { Result } from './result.model';

export interface Questionnaire {
    id: string;
    title: string;
    description: string;
    results: Result[];
    questions: Question[];
}
