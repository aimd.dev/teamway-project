export interface Result {
    id: string;
    title: string;
    description: string;
}
