import { TestBed } from '@angular/core/testing';

import { ErrorService } from './error.service';

describe('ErrorService', () => {
  let service: ErrorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a new error', () => {
    service.add('Sample error');
    service.errors$.subscribe(errors => {
      expect(errors.length).toBe(1);
    });
  });

  it('should remove an error', () => {
    service.remove(1);
    service.errors$.subscribe(errors => {
      expect(errors.length).toBe(0);
    });
  });
});
