import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private _loading$: Subject<boolean> = new Subject<boolean>();
  loading$: Observable<boolean> = this._loading$.asObservable();

  setLoading(loading: boolean) {
    this._loading$.next(loading);
  }
}
