import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private _errors$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  errors$: Observable<string[]> = this._errors$.asObservable();

  add(message: string) {
    const current = this._errors$.getValue();
    this._errors$.next([ ...current, message ]);
  }

  remove(index: number) {
    const current = this._errors$.getValue();
    current.splice(index, 1);
    this._errors$.next([ ...current ]);
  }
}
