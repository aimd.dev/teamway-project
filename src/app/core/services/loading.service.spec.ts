import { TestBed } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  let service: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should start loading', () => {
    service.setLoading(true);
    service.loading$.subscribe(loading => {
      expect(loading).toBe(true);
    });
  });

  it('should end loading', () => {
    service.setLoading(false);
    service.loading$.subscribe(loading => {
      expect(loading).toBe(false);
    });
  });
});
