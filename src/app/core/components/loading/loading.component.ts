import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingComponent implements OnInit {
  /**
   * Loading stream
   */
  loading$!: Observable<boolean>;
  /**
   * Component constructor
   * @param service loading service
   */
  constructor(private service: LoadingService) {}
  /**
   * Component on init
   */
  ngOnInit(): void {
    this.loading$ = this.service.loading$;
  }
}
