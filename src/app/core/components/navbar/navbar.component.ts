import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ErrorService } from '../../services/error.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  /**
   * Errors stream
   */
  errors$!: Observable<string[]>;
  /**
   * Component constructor
   * @param errorService error service
   */
  constructor(
    private errorService: ErrorService
  ) { }
  /**
   * Component on init
   */
  ngOnInit(): void {
    this.errors$ = this.errorService.errors$;
  }
  /**
   * Remove error
   * @param index error index
   */
  dismiss(index: number) {
    this.errorService.remove(index);
  }
}
