import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import { catchError, Observable, of, tap } from 'rxjs';
import { PersonalityTestService } from 'src/app/personality-test/personality-test.service';
import { Questionnaire } from 'src/app/shared/models';
import { ErrorService } from '../services/error.service';
import { LoadingService } from '../services/loading.service';


@Injectable({
  providedIn: 'root'
})
export class QuestionnaireResolver implements Resolve<Questionnaire> {
  /**
   * Resolver constructor
   * @param router angular router
   * @param service module service
   * @param loading loading service
   */
  constructor(
    private router: Router,
    private service: PersonalityTestService,
    private loading: LoadingService,
    private error: ErrorService
  ) { }
  /**
   * Handle error
   */
  handleError() {
    this.router.navigate(['/']);
    this.error.add('Not found. Please refresh and try again.');
  }
  /**
   * Route resolver
   * @param route activated route
   * @returns selected questionnaire
   */
  resolve(route: ActivatedRouteSnapshot): Observable<Questionnaire> {
    this.loading.setLoading(true);
    const id = route.params['questionnaire'];
    const extras = this.router.getCurrentNavigation()?.extras.state as Questionnaire;
    let response;
    // Fetch from firestore if the data is not in the state
    if (extras) { response = of(extras); }
    else { response = this.service.getQuestionnaire(id); }
    return response.pipe(
      tap((res) => {
        this.loading.setLoading(false);
        if (!res.title) { this.handleError(); }
      }),
      catchError(() => {
        this.handleError();
        throw 'Not found';
      })
    );
  }
}
