import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, tap } from 'rxjs';

import { PersonalityTestService } from 'src/app/personality-test/personality-test.service';
import { Questionnaire } from 'src/app/shared/models';
import { LoadingService } from '../services/loading.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireListResolver implements Resolve<Questionnaire[]> {
  /**
   * Resolver constructor
   * @param service module service
   * @param loading loading service
   */
  constructor(
    private service: PersonalityTestService,
    private loading: LoadingService
  ) { }
  /**
   * Route resolver
   * @returns questionnaire list
   */
  resolve(): Observable<Questionnaire[]> {
    this.loading.setLoading(true);
    return this.service.getQuestionnaires().pipe(tap(()=> this.loading.setLoading(false)));
  }
}
